//
//  WordImageCell.swift
//  Dictionary
//
//  Created by roman on 2020/08/10.
//  Copyright © 2020 roman. All rights reserved.
//

import UIKit
import Kingfisher

class TranslationCell: UITableViewCell {
    @IBOutlet weak var wordLabel: UILabel?
    @IBOutlet weak var wordImageView: UIImageView?
    
    func loadImageAsync(from url: URL) {
        guard let imageView = wordImageView else { return }
        
        let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
            |> RoundCornerImageProcessor(cornerRadius: 20)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: url,
            placeholder: UIImage(named: "imagePlaceholder"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(_):
                debugPrint("Image loaded successfully")
            case .failure(let error):
                debugPrint("Error loading image: \(error.localizedDescription)")
            }
        }
    }
}
