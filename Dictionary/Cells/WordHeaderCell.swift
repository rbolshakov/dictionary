//
//  WordHeaderCell.swift
//  Dictionary
//
//  Created by roman on 2020/08/10.
//  Copyright © 2020 roman. All rights reserved.
//

import UIKit
class WordHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel?
}
