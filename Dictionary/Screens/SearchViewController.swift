//
//  ViewController.swift
//  Dictionary
//
//  Created by roman on 2020/08/08.
//  Copyright © 2020 roman. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {

    let searchController = UISearchController(searchResultsController: nil)
    var words: [Word] = []
    var lastSearchString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
    }
    
    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        //searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let path = tableView.indexPathForSelectedRow else { return }
        let viewController = segue.destination as! WordViewController
        viewController.word = words[path.row]
    }
    
    //MARK: Data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = words[indexPath.row].text
        return cell
    }
}

extension SearchViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    guard let text = searchBar.text else { return }
    lastSearchString = text
    NetworkManager.sharedInstance.getWords(searchString: text) {[weak self] (searchString, words) in
        guard let self = self, searchString == self.lastSearchString else { return }
        self.words = words
        self.tableView.reloadData()
    }
  }
}
