//
//  WordViewController.swift
//  Dictionary
//
//  Created by roman on 2020/08/08.
//  Copyright © 2020 roman. All rights reserved.
//

import UIKit
import Kingfisher

class WordViewController: UITableViewController {
    
    var word: Word?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = word?.text
    }
    
    //MARK: Data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
            case 0:
                return 1
            default:
                return word?.meanings.count ?? 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! WordHeaderCell
            cell.titleLabel?.text = word?.text
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "translationCell", for: indexPath) as! TranslationCell
            if let text = word?.meanings[indexPath.row].translation?.text {
                cell.wordLabel?.text = "\(indexPath.row + 1). \(text)"
            }
            if let imageUrl = word?.meanings[indexPath.row].imageUrl {
                cell.loadImageAsync(from: imageUrl)
            }
            return cell
        }
    }
}
