import Foundation
import Alamofire

class NetworkManager {
    static let sharedInstance = NetworkManager()
    let search_url = "https://dictionary.skyeng.ru/api/public/v1/words/search"
    
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    func getWords(searchString: String, completionHandler: @escaping (String, [Word]) -> Void) {
        AF.request(search_url,
           method: .get,
           parameters: ["search": searchString],
           encoder: URLEncodedFormParameterEncoder.default).responseDecodable(of: [Word].self) { response in
            guard let value = response.value else {
                debugPrint(response.error ?? "Unknown error")
                return
            }
            debugPrint("result: \(value)")
            completionHandler(searchString, value)
        }
    }
}
