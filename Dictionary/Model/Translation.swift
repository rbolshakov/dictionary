//
//  Translation.swift
//  Dictionary
//
//  Created by roman on 2020/08/10.
//  Copyright © 2020 roman. All rights reserved.
//

import Foundation

class Translation: Codable {
    var text: String?
    var note: String?
}
