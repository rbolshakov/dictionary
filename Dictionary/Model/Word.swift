//
//  Word.swift
//  Dictionary
//
//  Created by roman on 2020/08/09.
//  Copyright © 2020 roman. All rights reserved.
//

import Foundation

class Word: Codable {
    var wordId: Int?
    var text: String?
    var meanings: [Meaning] = []
    
    enum CodingKeys: String, CodingKey {
        case wordId = "id"
        case text = "text"
        case meanings = "meanings"
    }
}
