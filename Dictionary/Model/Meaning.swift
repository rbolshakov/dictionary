//
//  Meaning.swift
//  Dictionary
//
//  Created by roman on 2020/08/10.
//  Copyright © 2020 roman. All rights reserved.
//

import Foundation

class Meaning: Codable {
    var meaningId: Int?
    var rawUrl: String?
    var translation: Translation?
    var imageUrl: URL? {
        if let url = rawUrl {
            return URL(string: "https:\(url)")
        } else {
            return nil
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case meaningId = "id"
        case rawUrl = "imageUrl"
        case translation = "translation"
    }
}
