//
//  DictionaryTests.swift
//  DictionaryTests
//
//  Created by roman on 2020/08/08.
//  Copyright © 2020 roman. All rights reserved.
//

import XCTest
@testable import Dictionary

class DictionaryTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCompUrlIsNotNilIfImageUrlExist() throws {
        let meaning = Meaning()
        meaning.rawUrl = "https://www.yandex.ru"
        XCTAssertNotNil(meaning.imageUrl)
    }
    
    func testCompUrlIsNilIfImageUrlMissing() throws {
        let meaning = Meaning()
        meaning.rawUrl = nil
        XCTAssertNil(meaning.imageUrl)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
