# Dictionary

Dictionary is a simple dictionary app powered by Skyeng API.

### Third-part libraries

Dictionary uses a number of open source projects to work properly:

* [Alamofire](https://github.com/Alamofire/Alamofire) - for simplifying network requests
* [Kingfisher](https://github.com/onevcat/Kingfisher) - for loading images asynchronously

### Installation

Dictionary requires [Cocoapods](https://cocoapods.org/) to build it.

Install the app dependencies.

```sh
$ pod install
```
Then open Dictionary.xcworkspace in XCode and run the project.
